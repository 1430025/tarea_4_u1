/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.clases;

import java.util.Objects;

/**
 *
 * @author Profesor
 */
public class Teclado {
    static int n =0;
    private int id;
    private String numeroDeSerie;
    private String marca;
    private String modelo;
    private String color;
    private double tamano;
    public Teclado(String a,String b, String c,String d,double e){
       this.numeroDeSerie = a;
       this.marca = b;
       this.modelo = c;
       this.color = d;
       this.tamano = e;
       this.id = n;
       n++;
    }

    @Override
    public String toString(){
       return this.id+","+this.numeroDeSerie+","+this.marca+","+
               this.modelo+","+this.color+","+this.tamano;
    }


    @Override
    public boolean equals(Object other){
         Monitor monitor = (Monitor)other;
         return monitor.getNumeroDeSerie().equals(this.numeroDeSerie) &&
         monitor.getModelo().equals(this.modelo) &&
                 monitor.getMarca().equals(this.marca);
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + this.id;
        hash = 29 * hash + Objects.hashCode(this.numeroDeSerie);
        hash = 29 * hash + Objects.hashCode(this.marca);
        hash = 29 * hash + Objects.hashCode(this.modelo);
        hash = 29 * hash + Objects.hashCode(this.color);
        hash = 29 * hash + (int) (Double.doubleToLongBits(this.tamano) ^ (Double.doubleToLongBits(this.tamano) >>> 32));
        return hash;
    }
    
}
